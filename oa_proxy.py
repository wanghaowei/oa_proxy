
from flask import Flask, request, json, jsonify
from zeep import Client


client = Client(
    'http://oa.chinahoroy.com/sys/webservice/sysNotifyTodoWebService?wsdl')

app = Flask(__name__)


@app.route("/add_todo", methods=['POST'])
def add_todo():
    data = request.get_json()
    targets = json.dumps([data['target']])
    result = client.service.sendTodo({
        'appName': '设享云',
        'modelName': data['modelName'],
        'modelId': str(data['modelId']),
        'subject': data['subject'],
        'link': data['link'],
        'type': 2,
        'createTime': data['createTime'],
        'targets': targets,
    })
    return jsonify({'message': result['message'], 'returnState': result['returnState']})


@app.route("/finish_todo", methods=['POST'])
def finish_todo():
    data = request.get_json()
    targets = json.dumps([data['target']])
    result = client.service.setTodoDone({
        'appName': '设享云',
        'modelName': data['modelName'],
        'modelId': str(data['modelId']),
        'optType': 1,
        'type': 2,
        'targets': targets,
    })
    return jsonify({'message': result['message'], 'returnState': result['returnState']})
